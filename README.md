# Coriolis

> the apparent acceleration of a moving body on or near the Earth as a result of the Earth's rotation

## Use

Install collections and 3rd party roles.

``` bash
ansible-galaxy collection install -r requirements.yaml
ansible-galaxy role install -r requirements.yaml --roles-path roles/
```

For passphrase protected keys: `ssh-agent`; even better via [keychain](https://www.funtoo.org/Keychain)

``` bash
ssh-agent bash
ssh-add ~/.ssh/id_rsa
```

Assuming same sudo password on *all* targeted machines.

``` bash
ansible-playbook cloud.yaml -i inventory.yaml -K
```

Should now see something like:

``` bash
...
PLAY RECAP **********************************************************************************************************
node0                      : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
node1                      : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
node2                      : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

For one-off commands on all machines as root, try:

``` bash
ansible cloud -i inventory.yaml -a "sudo apt-get update --yes" --become -K
ansible cloud -i inventory.yaml -a "sudo apt-get upgrade --yes" --become -K
ansible cloud -i inventory.yaml -a "/sbin/reboot" --become -K
ansible cloud -i inventory.yaml -a "sudo apt-get autoremove --yes" --become -K
ansible cloud -i inventory.yaml -a "sudo do-release-upgrade -f DistUpgradeViewNonInteractive" --become -K
```

## Reading

* [How To Install and Configure Ansible on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-20-04)
* [Ansible and HashiCorp: Better Together](https://www.hashicorp.com/resources/ansible-terraform-better-together)
* [How to manage your workstation configuration with Ansible](https://opensource.com/article/18/3/manage-workstation-ansible)
* [Ansible Docs » User Guide » Introduction to ad hoc commands](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html) 

## License

Ethically sourced under the [Atmosphere License](https://www.open-austin.org/atmosphere-license/)—like open source, for good.
